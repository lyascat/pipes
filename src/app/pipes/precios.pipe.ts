import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'precios'
})
export class PreciosPipe implements PipeTransform {

  transform(value: number, entero = true): unknown {
    const moneda = 'S/. ';
    const point = '.';
    const valor = value.toString();
    const numeros = valor.split('.');

    if (entero) {
      return moneda + numeros[0];
    } else {
      return point + numeros[1];
    }
  }

}
